<?php

$_['heading_title']    = '<i style="font-size: 24px;position: relative;top: 2px;color: #fa5eff" class="fa fa-flask" aria-hidden="true"></i> Bazzilla';
$_['heading_inner'] = 'Bazzilla - Личный кабинет';

$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_banner']     = 'Баннер';
$_['entry_dimension']  = 'Размеры (Ширина x Высота)';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_name']       = 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width']      = 'Введите ширину изображения!';
$_['error_height']     = 'Введите высоту изображения!';