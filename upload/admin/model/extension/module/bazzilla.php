<?php

class ModelExtensionModuleBazzilla extends Model {
    public function install() {
        $this->db->query("ALTER TABLE `".DB_PREFIX."category` ADD is_bazzilla tinyint(1)");
        $this->db->query("ALTER TABLE `".DB_PREFIX."product` ADD bazzilla_id tinyint(1)");
        $this->db->query("ALTER TABLE `".DB_PREFIX."product` ADD INDEX `bazzilla_product_id_index` (`bazzilla_id`)");
    }

    public function uninstall() {
        $this->db->query("ALTER TABLE `".DB_PREFIX."category` DROP `is_bazzilla`");
        $this->db->query("ALTER TABLE `".DB_PREFIX."product` DROP `bazzilla_id`");
        $this->db->query("ALTER TABLE `".DB_PREFIX."product` DROP INDEX `bazzilla_product_id_index`");
    }
}