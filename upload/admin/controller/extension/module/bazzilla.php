<?php

class ControllerExtensionModuleBazzilla extends Controller
{
    private $categoryMap = array();

    public function index()
    {
        $this->load->language('extension/module/bazzilla');

        $this->document->setTitle($this->language->get('heading_inner'));

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $isError = false;

            $this->load->model('extension/module/bazzilla');

            if (isset($this->request->post['bazzilla_api_token'])) {
                $bazzillaToken = $this->request->post['bazzilla_api_token'];

                $bazzillaAccount = new \bazzilla\Bazzilla();

                try {
                    $this->model_extension_module_bazzilla->install();
                } catch(Exception $ex) {

                }
            

                try {
                    $bazzillaAccount->login($bazzillaToken);
                    $this->model_setting_setting->editSetting('hidden_bazzilla', [
                        'hidden_bazzilla_api_token' => $bazzillaToken,
                    ]);
                } catch (\bazzilla\exceptions\InvalidLoginException $exception) {
                    $isError               = true;
                    $data['error_warning'] = 'Неправильный токен';
                }
            }

            if ( ! $isError) {
                $this->updateSettings();
            }
        }

        if (isset($this->request->post['bazzilla_status'])) {
            $data['bazzilla_status'] = $this->request->post['bazzilla_status'];
        } else {
            $data['bazzilla_status'] = $this->config->get('bazzilla_status');
        }

        if (isset($this->request->post['bazzilla_api_token'])) {
            $data['bazzilla_api_token'] = $this->request->post['bazzilla_api_token'];
        } else {
            $data['bazzilla_api_token'] = $this->config->get('hidden_bazzilla_api_token');
        }

        if (isset($this->request->post['bazzilla_inventory'])) {
            $data['bazzilla_inventory'] = $this->request->post['bazzilla_inventory'];
        } else {
            $data['bazzilla_inventory'] = $this->config->get('bazzilla_inventory');
        }

        if (isset($this->request->post['bazzilla_per_request'])) {
            $data['bazzilla_per_request'] = $this->request->post['bazzilla_per_request'];
        } else {
            $data['bazzilla_per_request'] = $this->config->get('bazzilla_per_request');
        }

        $data['is_invalid_session'] = false;

        $token = $this->config->get('hidden_bazzilla_api_token');

        $bazzillaAccount = new \bazzilla\Bazzilla();

        if ($token) {
            try {
                $bazzillaAccount->login($token);
            } catch (\bazzilla\exceptions\InvalidLoginException $exception) {
                $data['is_invalid_session'] = true;
            }
        } else {
            $data['is_invalid_session'] = true;
        }

        if ( ! $data['is_invalid_session']) {
            try {
                $data['inventories'] = $bazzillaAccount->getInventories();
            } catch (\bazzilla\exceptions\InvalidSessionException $ex) {
                $data['is_invalid_session'] = true;
            }
        }

        $data['breadcrumbs'] = $this->generateBreadcrumbs();
        $data['action']      = $this->url->link('extension/module/bazzilla',
            'user_token=' . $this->session->data['user_token'], true);
        $data['logout']      = $this->url->link('extension/module/bazzilla/logout',
            'user_token=' . $this->session->data['user_token'], true);
        $data['categories']  = $this->url->link('extension/module/bazzilla/categories',
            'user_token=' . $this->session->data['user_token'], true);
        $data['cancel']      = $this->url->link('marketplace/extension',
            'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        if ( ! isset($this->request->get['module_id'])) {
            $data['action_clear'] = $this->url->link('extension/module/bazzilla/clear',
                'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action_clear'] = $this->url->link('extension/module/bazzilla/clear',
                'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'],
                true);
        }

        if ( ! isset($this->request->get['module_id'])) {
            $data['action_categories'] = $this->url->link('extension/module/bazzilla/updateCategories',
                'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action_categories'] = $this->url->link('extension/module/bazzilla/updateCategories',
                'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'],
                true);
        }

        $this->response->setOutput($this->load->view('extension/module/bazzilla', $data));
    }

    public function logout()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('hidden_bazzilla', []);
        $action = $this->url->link('extension/module/bazzilla', 'user_token=' . $this->session->data['user_token'],
            true);
        $this->response->redirect($action);
    }

    public function clear()
    {
        $this->clearCategories();
        $this->deleteAllProducts();
        /**
         * Удаляем файлы
         */
        foreach(glob(DIR_IMAGE . 'catalog/bazzilla/*') as $file) {
            unlink($file);
        }
        foreach(glob(DIR_IMAGE . 'cache/catalog/bazzilla/*') as $file) {
            unlink($file);
        }

        echo json_encode([
           'success' => true,
        ]);
    }

    private function updateSettings()
    {
        $this->load->language('extension/module/bazzilla');
        $this->model_setting_setting->editSetting('bazzilla', $this->request->post);
        $this->session->data['success'] = $this->language->get('text_success');
        $action = $this->url->link('extension/module/bazzilla', 'user_token=' . $this->session->data['user_token'],
            true);
        $this->response->redirect($action);
    }

    private function clearCategories()
    {
        $categoriesQuery = $this->db->query("SELECT category_id, is_bazzilla FROM " . DB_PREFIX . "category WHERE is_bazzilla = 1");
        $categories = $categoriesQuery->rows;

        $categoriesIds = [];

        foreach ($categories as $category) {
            $categoriesIds[]  = strval($category['category_id']);
        }

        if (count($categoriesIds) == 0) return;

        $categoriesIdsStr = implode(', ', $categoriesIds);

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_path WHERE category_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id IN ($categoriesIdsStr)");
    }

    private function deleteAllProducts()
    {

        $categoriesQuery = $this->db->query("SELECT product_id, bazzilla_id FROM " . DB_PREFIX . "product WHERE bazzilla_id IS NOT NULL");
        $categories = $categoriesQuery->rows;

        $categoriesIds = [];

        foreach ($categories as $category) {
            $categoriesIds[]  = strval($category['product_id']);
        }

        if (count($categoriesIds) == 0) return;

        $categoriesIdsStr = implode(', ', $categoriesIds);

        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id IN ($categoriesIdsStr)");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id IN ($categoriesIdsStr)");
    }

    private function generateBreadcrumbs()
    {
        $this->load->language('extension/module/bazzilla');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension',
                'user_token=' . $this->session->data['user_token'] . '&type=module', true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/bazzilla', 'user_token=' . $this->session->data['user_token'],
                true),
        );

        return $data;
    }

    private function validate()
    {
        return true;
    }

    public function updateCategories()
    {
        $xmlstr = file_get_contents(DIR_APPLICATION . 'bc.xml');
        $xml    = new SimpleXMLElement($xmlstr);
        $this->addCategories($xml->shop->categories);

        echo json_encode([
            'success' => true,
        ]);
    }

    private function addCategories($categories)
    {
        $this->load->model('tool/import_yml_bazzilla');
        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/attribute');
        $this->load->model('catalog/attribute_group');
        $this->load->model('localisation/language');
        $this->load->model('setting/setting');

        $this->categoryMap[0] = array(
            'category_id' => 0,
            'name'        => 0,
        );

        $categoriesList = array();

        foreach ($categories->category as $category) {
            $categoriesList[(string)$category['id']] = array(
                'parent_id' => (int)$category['parentId'],
                'name'      => trim((string)$category),
            );
        }

        // Compare categories level by level and create new one, if it doesn't exist
        while (count($categoriesList) > 0) {
            $previousCount = count($categoriesList);

            foreach ($categoriesList as $source_category_id => $item) {



                if (array_key_exists((int)$item['parent_id'], $this->categoryMap)) {

                    $category = $this->model_tool_import_yml_bazzilla->loadCategory($this->categoryMap[$item['parent_id']]['category_id'],
                        $item['name']);
                    if ($category->row) {
                        $this->categoryMap[(int)$source_category_id] = array(
                            'category_id' => $category->row['category_id'],
                            'name'        => $item['name'],
                        );
                    } else {
                        $category_data = array(
                            'sort_order'     => 0,
                            'parent_id'      => $this->categoryMap[(int)$item['parent_id']]['category_id'],
                            'top'            => 0,
                            'status'         => 1,
                            'noindex'        => 1,
                            'column'         => '',
//              'category_description' => array (
//                1 => array(
//                  'name' => $item['name'],
//                  'meta_title' => $item['name'],
//                  'meta_h1' => $item['name'],
//                  'meta_keyword' => '',
//                  'meta_description' => '',
//                  'description' => '',
//                )
//              ),
                            'keyword'        => $this->translitText($item['name']),
                            'category_store' => array(
                                0,
                            ),
                        );

                        $languages = $this->model_localisation_language->getLanguages();

                        foreach ($languages as $language) {
                            $category_data['category_description'][$language['language_id']] = [
                                'name'             => $item['name'],
                                'meta_title'       => $item['name'],
                                'meta_h1'          => $item['name'],
                                'meta_keyword'     => '',
                                'meta_description' => '',
                                'description'      => '',
                            ];
                        }

                        if ($category_data['parent_id'] == 0) {
                            $category_data['top'] = 1;
                        }

                        $this->categoryMap[(int)$source_category_id] = array(
                            'category_id' => $this->addCategory($category_data),
                            'name'        => $item['name'],
                        );
                    }
                    unset($categoriesList[$source_category_id]);
                }
            }

            if (count($categoriesList) === $previousCount) {
                break;
                //echo("Unliked tree path:\n");
                //print_r($categoriesList);
                //die();
            }
        }
    }

    private function addCategory($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET is_bazzilla = '1', parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if ( ! empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');

        return $category_id;
    }

    private function translitText($string)
    {
        $replace = array(
            "А"  => "A",
            "а"  => "a",
            "Б"  => "B",
            "б"  => "b",
            "В"  => "V",
            "в"  => "v",
            "Г"  => "G",
            "г"  => "g",
            "Д"  => "D",
            "д"  => "d",
            "Е"  => "E",
            "е"  => "e",
            "Ё"  => "E",
            "ё"  => "e",
            "Ж"  => "Zh",
            "ж"  => "zh",
            "З"  => "Z",
            "з"  => "z",
            "И"  => "I",
            "и"  => "i",
            "Й"  => "I",
            "й"  => "i",
            "К"  => "K",
            "к"  => "k",
            "Л"  => "L",
            "л"  => "l",
            "М"  => "M",
            "м"  => "m",
            "Н"  => "N",
            "н"  => "n",
            "О"  => "O",
            "о"  => "o",
            "П"  => "P",
            "п"  => "p",
            "Р"  => "R",
            "р"  => "r",
            "С"  => "S",
            "с"  => "s",
            "Т"  => "T",
            "т"  => "t",
            "У"  => "U",
            "у"  => "u",
            "Ф"  => "F",
            "ф"  => "f",
            "Х"  => "H",
            "х"  => "h",
            "Ц"  => "Tc",
            "ц"  => "tc",
            "Ч"  => "Ch",
            "ч"  => "ch",
            "Ш"  => "Sh",
            "ш"  => "sh",
            "Щ"  => "Shch",
            "щ"  => "shch",
            "Ы"  => "Y",
            "ы"  => "y",
            "Э"  => "E",
            "э"  => "e",
            "Ю"  => "Iu",
            "ю"  => "iu",
            "Я"  => "Ia",
            "я"  => "ia",
            "ъ"  => "",
            "ь"  => "",
            "«"  => "",
            "»"  => "",
            "„"  => "",
            "“"  => "",
            "“"  => "",
            "”"  => "",
            "\•" => "",
            "%"  => "",
            "$"  => "",
            "_"  => "-",
            " "  => "-",
            "."  => "-",
            ","  => "-",
            "І"  => "I",
            "і"  => "i",
            "Ї"  => "Yi",
            "ї"  => "yi",
            "Є"  => "Ye",
            "є"  => "ye",
        );

        $output = iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
        $output = strtolower($output);
        $output = preg_replace('~[^-a-z0-9_]+~u', '-', $output);
        $output = trim($output, "-");
        $output = str_replace("----", "-", $output);
        $output = str_replace("---", "-", $output);
        $output = str_replace("--", "-", $output);

        return $output;
    }
}