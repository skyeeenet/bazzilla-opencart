<?php

class ControllerExtensionModuleBazzillaSync extends Controller
{
    public function index()
    {
        $this->load->model('extension/module/bazzilla');
        $this->load->model('catalog/category');
        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $bazzillaCatalogImage = 'catalog/bazzilla';
        $offset               = $this->request->get['offset'];
        $limit                = $this->config->get('bazzilla_per_request');

        $token               = $this->config->get('hidden_bazzilla_api_token');
        $bazzillaInventoryId = $this->config->get('bazzilla_inventory');

        $bazzillaAccount = new \bazzilla\Bazzilla();

        try {
            $bazzillaAccount->fastLogin($token);
            $bazzillaAccount->setInventoryId($bazzillaInventoryId);
            $bazzillaProducts = $bazzillaAccount->getProducts($limit, $offset);
        } catch (Exception $exception) {
            
            echo json_encode([
                'success' => false,
                'count'   => 0,
            ]);

            return;
        }

        if ($offset == 0) {
            if ( ! is_dir(DIR_IMAGE . $bazzillaCatalogImage)) {
                mkdir(DIR_IMAGE . $bazzillaCatalogImage, 0777, true);
            }
            $this->model_extension_module_bazzilla->updateProductsStatus(0);
        }

        foreach ($bazzillaProducts as $bazzillaProduct) {
            $productImages      = $this->simpleProductImages($bazzillaProduct['images']);
            $bazzillaCategories = $bazzillaProduct['category'];
            $productId          = $bazzillaProduct['id'];
            $productActive      = $bazzillaProduct['inStock'];
            $productTitle       = $bazzillaProduct['title'];
            $productName        = $bazzillaProduct['title'];
            $productDescription = $bazzillaProduct['description'];
            $productMpn         = $bazzillaProduct['mpn'];
            $productBrand       = $bazzillaProduct['brand']['title'];
            $productPrice       = $bazzillaProduct['price'];

            $productBrand = $this->db->escape($productBrand);

            $storeProduct   = $this->model_extension_module_bazzilla->getProduct($productId);
            $manufacturerId = $this->model_extension_module_bazzilla->createOrFirstManufacturer($productBrand);

            //$this->model_extension_module_bazzilla->updateManufacturer($storeProduct['product_id'], $manufacturerId);

            if ($storeProduct && $storeProduct['price'] != $productPrice) {
                $this->model_extension_module_bazzilla->updateProductPrice($productId, $productPrice);
            }

            if ($storeProduct && $productActive == true) {
                $this->model_extension_module_bazzilla->updateProductStatus(1, $productId);
            }

            if ($storeProduct && $productActive == false) {
                $this->model_extension_module_bazzilla->deleteProduct($storeProduct['product_id']);
                if ($storeProduct['image']) {
                    unlink(DIR_IMAGE . "$bazzillaCatalogImage/" . $storeProduct['image']);
                }
            }

            if ( ! $storeProduct && $productActive == 1) {

                $category = $this->getCategory($bazzillaCategories);

                if (count($productImages) > 0) {
                    $productMainImage  = array_shift($productImages);
                    $productInnerImage = explode('/', $productMainImage);
                    file_put_contents(DIR_IMAGE . "$bazzillaCatalogImage/" . $productInnerImage[count($productInnerImage) - 1],
                        file_get_contents($productMainImage));
                    $productImagePath = "$bazzillaCatalogImage/" . $productInnerImage[count($productInnerImage) - 1];
                } else {
                    $productImagePath = '';
                }

                $storeProductId = $this->model_extension_module_bazzilla->createProduct([
                    'status'           => intval($productActive),
                    'sort_order'       => 0,
                    'price'            => $productPrice,
                    'quantity'         => 999,
                    'image'            => $productImagePath,
                    'mpn'              => $productMpn,
                    'sku'              => $productMpn,
                    'manufacturer_id'  => $manufacturerId,
                    'upc'              => $productMpn,
                    'isbn'             => $productMpn,
                    'ean'              => $productMpn,
                    'minimum'          => 1,
                    'jan'              => $productMpn,
                    'model'            => $productBrand,
                    'languages'        => $languages,
                    'name'             => $this->db->escape($productName),
                    'description'      => $this->db->escape($productDescription),
                    'tag'              => '',
                    'meta_title'       => $this->db->escape($productTitle),
                    'meta_description' => $this->db->escape($productDescription),
                    'meta_keyword'     => $this->db->escape($productTitle),
                    'bazzilla_id'      => $productId,
                    'category_id'      => $category,
                ]);

                foreach ($productImages as $productImage) {
                    $productInnerImage = explode('/', $productImage);
                    file_put_contents(DIR_IMAGE . "$bazzillaCatalogImage/" . $productInnerImage[count($productInnerImage) - 1],
                        file_get_contents($productImage));
                }

                foreach ($productImages as $productImage) {
                    $productInnerImage = explode('/', $productImage);
                    $this->model_extension_module_bazzilla->addProductImage($storeProductId,
                        "$bazzillaCatalogImage/" . $productInnerImage[count($productInnerImage) - 1]);
                }
            }
        }

        echo json_encode([
            'success' => true,
            'count'   => count($bazzillaProducts),
        ]);
    }

    private function simpleProductImages($images)
    {
        $simpleImages = [];

        foreach ($images as $image) {
            $simpleImages[] = $image['url'];
        }

        return $simpleImages;
    }

    private function getCategory($categories)
    {
        //$categoriesParts = explode(' > ', $categories);
        //$category        = array_pop($categoriesParts);

        if (!$categories) {
            return 0;
        }

        $category   = $this->db->escape($categories['title']);
        $languageId = (int)$this->config->get('config_language_id');

        $query       = $this->db->query("SELECT name, category_id, language_id FROM " . DB_PREFIX . "category_description WHERE language_id = '$languageId' AND name = '$category'");
        $categoryObj = $query->row;

        if ($categoryObj) {
            return $categoryObj['category_id'];
        } else {
            return 0;
        }
    }
}
