<?php

class ControllerExtensionModuleBazzilla extends Controller
{
    private $categories = [];

    public function index()
    {

    }

    public function updateCategories()
    {


//        $bazzillaCategories = $this->getCategories();
//        $languageId         = (int)$this->config->get('config_language_id');
//
//        foreach ($bazzillaCategories as $bazzillaCategory) {
//            $this->syncCategory($bazzillaCategory[0]);
//        }

        echo 'success';
    }

    private function getCategories()
    {
        $bazzillaCategories = file_get_contents(CENTER_GET_CATEGORIES);
        $bazzillaCategories = json_decode($bazzillaCategories, true)['payload'];

        return $bazzillaCategories;
    }

    private function syncCategory($categories)
    {
        $languageId = (int)$this->config->get('config_language_id');
        $this->load->model('catalog/category');
        $this->load->model('localisation/language');
        $this->load->model('setting/store');

        $partCategories = explode('/', $categories);

        $parentCategoryId = 0;

        foreach ($partCategories as $index => $categoryName) {

            $query      = $this->db->query("SELECT c.category_id, cd.name, cd.language_id FROM " . DB_PREFIX . "category_description cd LEFT JOIN " . DB_PREFIX . "category c ON (c.category_id = cd.category_id) WHERE c.parent_id = '" . $parentCategoryId . "' AND name = '" . $categoryName . "' AND language_id = '" . $languageId . "'");
            $dbCategory = $query->row;

            if ( ! $dbCategory) {
                $languages = $this->model_localisation_language->getLanguages();
                $stores    = $this->model_setting_store->getStores();

                $categoryDescription = [];

                foreach ($languages as $language) {

                    $categoryDescription[$language['language_id']] = [
                        'name'             => $categoryName,
                        'description'      => $categoryName,
                        'h1'               => $categoryName,
                        'meta_title'       => $categoryName,
                        'meta_description' => $categoryName,
                    ];
                }

                $categoryStores = [];

                foreach ($stores as $store) {
                    $categoryStores[] = $store['store_id'];
                }

                if (count($categoryStores) == 0) {
                    $categoryStores[] = 0;
                }

                $categoryId = $this->model_catalog_category->addCategory([
                    'parent_id'            => $parentCategoryId,
                    'is_bazzilla'          => 1,
                    'top'                  => 1,
                    'column'               => 1,
                    'sort_order'           => 0,
                    'status'               => 1,
                    'category_description' => $categoryDescription,
                    'category_store'       => $categoryStores,
                ]);

                $parentCategoryId = $categoryId;

            } else {
                $parentCategoryId = $dbCategory['category_id'];
            }

            if ($index == (count($partCategories) - 1)) {
                $parentCategoryId = 0;
            }

        }
    }

    private function createCategory($categoryName, $parentCategory, $languageId)
    {
        $this->load->model('catalog/category');
        $this->load->model('localisation/language');
        $this->load->model('setting/store');

        $parentCategoryId = 0;

        if ($parentCategory != null) {
            $parentCategoryId = $this->findCategoryInCache($parentCategory);
        }

        $query    = $this->db->query("SELECT category_id, name, language_id FROM " . DB_PREFIX . "category_description WHERE name = '" . $categoryName . "' AND language_id = '" . $languageId . "'");
        $category = $query->row;

        if ( ! $category) {

            $languages = $this->model_localisation_language->getLanguages();
            $stores    = $this->model_setting_store->getStores();

            $categoryDescription = [];

            foreach ($languages as $language) {

                $categoryDescription[$language['language_id']] = [
                    'name'             => $categoryName,
                    'description'      => $categoryName,
                    'h1'               => $categoryName,
                    'meta_title'       => $categoryName,
                    'meta_description' => $categoryName,
                ];
            }

            $categoryStores = [];

            foreach ($stores as $store) {
                $categoryStores[] = $store['store_id'];
            }

            if (count($categoryStores) == 0) {
                $categoryStores[] = 0;
            }

            $categoryId = $this->model_catalog_category->addCategory([
                'parent_id'            => $parentCategoryId,
                'is_bazzilla'          => 1,
                'top'                  => 1,
                'column'               => 1,
                'sort_order'           => 0,
                'status'               => 1,
                'category_description' => $categoryDescription,
                'category_store'       => $categoryStores,
            ]);

            $this->writeCategoryToCache($categoryName, $categoryId);

        } else {
            $this->writeCategoryToCache($categoryName, $category['category_id']);
        }
    }

    private function findCategoryInCache($categoryName)
    {
        return array_search($categoryName, $this->categories);
    }

    private function writeCategoryToCache($categoryName, $categoryId)
    {
        $this->categories[$categoryId] = $categoryName;
    }
}