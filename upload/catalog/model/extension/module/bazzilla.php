<?php

class ModelExtensionModuleBazzilla extends Model
{
    public function getLanguageId($lang)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE code = '" . $lang . "'");
        return $query->rows[0];
    }

    public function clearCategories()
    {
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category");
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category_description");
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category_path");
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category_filter");
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category_to_layout");
        $this->db->query("TRUNCATE TABLE '" . DB_PREFIX . "'category_to_store");
    }

    public function updateProductsStatus($status)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET status = '" . $status . "' WHERE bazzilla_id != 0");
    }

    public function updateProductStatus($status, $bazzilla_product_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET status = '" . $status . "' WHERE bazzilla_id = '" . $bazzilla_product_id . "'");
    }

    public function clearCategoriesToStore()
    {
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "category_to_store");
    }

    public function getCategory($category_id, $language_id)
    {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.bazzilla_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$language_id . "'");
        return $query->row;
    }

    public function updateParentId($category_id, $parent_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int)$parent_id . "' WHERE category_id = '" . (int)$category_id . "'");
    }

    public function getCategories()
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."category");
        return $query->rows;
    }

    public function createProduct($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET manufacturer_id = '".$data['manufacturer_id']."', model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', image = '".$data['image']."', bazzilla_id = '".$data['bazzilla_id']."', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', price = '" . (float)$data['price'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

        $product_id = $this->db->getLastId();

        foreach ($data['languages'] as $language) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', tag = '" . $this->db->escape($data['tag']) . "', meta_title = '" . $this->db->escape($data['meta_title']) . "', meta_description = '" . $this->db->escape($data['meta_description']) . "', meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "'");
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET main_category = 1, product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['category_id'] . "'");

        $storeId = $this->config->get('config_store_id') == null ? 0 : $this->config->get('config_store_id');
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$storeId . "'");

        $this->cache->delete('product');

        return $product_id;
    }

    public function deleteAllProducts()
    {
        $products = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE bazzilla_id IS NOT NULL");

        foreach($products->rows as $product) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . $product['product_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . $product['product_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . $product['product_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . $product['product_id'] . "'");
            $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . $product['product_id'] . "'");
        }
    }

    public function addProductImage($product_id, $image)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . $product_id . "', image = '" . $image . "', sort_order = '0'");
    }

    public function getProduct($bazzilla_product_id)
    {
        $query = $this->db->query("SELECT product_id, bazzilla_id, image, price FROM " . DB_PREFIX . "product WHERE bazzilla_id = '" . $bazzilla_product_id . "'");
        return $query->row;
    }

    public function deleteProduct($bazzilla_product_id)
    {
        $product = $this->getProduct($bazzilla_product_id);
        $productImages = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . $product['product_id'] . "'");

        foreach($productImages as $image) {
            /**
            * Удаляем файлы
            */
            unlink(DIR_IMAGE . 'bazzilla/' . $image['image']);
            unlink(DIR_IMAGE . 'cache/bazzilla/' . $image['image']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . $product['product_id'] . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . $product['product_id'] . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . $product['product_id'] . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . $product['product_id'] . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . $product['product_id'] . "'");
    }

    public function updateProductPrice($bazzilla_product_id, $price)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET price = '".$price."' WHERE bazzilla_id = '" . $bazzilla_product_id . "'");
    }

    public function createOrFirstManufacturer($brandName)
    {
        $manufacturer = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer WHERE name = '".$brandName."' LIMIT 1");
        $storeId = $this->config->get('config_store_id') == null ? 0 : $this->config->get('config_store_id');

        if ($manufacturer->rows) {
            return $manufacturer->row['manufacturer_id'];
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '" . $brandName . "'");
            $manufacturerId = $this->db->getLastId();
            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturerId . "', store_id = '" . (int)$storeId . "'");
            return $manufacturerId;
        }
    }

    public function updateManufacturer($product_id, $manufacturer_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET manufacturer_id = '" . $manufacturer_id . "' WHERE product_id = '" . $product_id . "'");
    }
}