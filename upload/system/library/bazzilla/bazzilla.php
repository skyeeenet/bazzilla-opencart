<?php

namespace bazzilla;

use bazzilla\services\bazzillaService;

class Bazzilla
{
    private $bazzillaService;
    private $token;
    private $inventoryId;

    public function __construct()
    {
        $this->bazzillaService = new BazzillaService();
    }

    public function setInventoryId($inventoryId)
    {
        $this->inventoryId = $inventoryId;
    }

    public function login($token)
    {
        $this->token = $this->bazzillaService->login($token);
    }

    public function fastLogin($token)
    {
        $this->token = $token;
    }

    public function getInventories()
    {
        return $this->bazzillaService->getInventories($this->token);
    }

    public function getProducts(int $limit, int $offset)
    {
        return $this->bazzillaService->getProducts($this->token, $this->inventoryId, $limit, $offset);
    }
}