<?php

namespace Bazzilla\Services;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use Bazzilla\Exceptions\InvalidLoginException;
use Bazzilla\Exceptions\InvalidProductsException;
use Bazzilla\Exceptions\InvalidSessionException;

class BazzillaService
{
    private $requestServer = 'https://v2.bazzilla.com.ua/api/v1/';

    public function __construct()
    {
        // $this->soapClient = new \SoapClient('http://www.bazzilla.com.ua/merchants/api12/wsdl', [
        //     'trace' => 1,
        // ]);
    }

    public function login($token): string
    {
        try {
            $this->getInventories($token, 0, 1);
        } catch (InvalidSessionException $ex) {
            throw new InvalidLoginException();
        }

        return $token;
    }

    public function getInventories(string $token, $offset = 0, $limit = 20): array
    {
        $authorization = "Authorization: Bearer $token";
        $ch            = curl_init($this->requestServer . "inventories?offset=$offset&limit=$limit");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $inventories = json_decode($result, true);

        if ($inventories) {
            return $inventories;
        } else {
            
            throw new InvalidSessionException();
        }
    }

    public function getProducts(string $token, string $inventoryId, int $limit, int $offset): array
    {
        $authorization = "Authorization: Bearer $token";
        $ch            = curl_init($this->requestServer . "products?offset=$offset&limit=$limit&inventoryId=$inventoryId");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $products = json_decode($result, true);

        if ($products) {
            return $products;
        } else {
            throw new InvalidSessionException();
        }
    }
}